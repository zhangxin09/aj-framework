# AJ-Framework Global Readme

A Web Framework for Spring Boot has these features:

- Designed for distributed use, suitable for high-concurrency scenarios
- Lightweight, minimize dependencies, less code, more functionality
- Full-stack, support both front-end and back-end, Java and TypeScript
- Clear code, Modularity and low coupling
- Fully documented and tested, easy to understand and maintain

## Tech stack

- JDK11/Tomcat 9/Spring Boot 2.7/JPA
- Dubbo 3.3 RPC API/Redis 6/Redisson
- Spring-native build with GraalVM

# Core Library

# Components

| Name                                                  | Detail             |
|-------------------------------------------------------|--------------------|
| [Delayed Task Queue](/components/aj-delayedtaskqueue) | Delayed Task Queue |
| [Desensitize](/components/aj-desensitize)             | Data Desensitize   |
| [Monitor](/components/aj-monitor)                     | Spring Monitor     |
| [Trace](/components/aj-trace)                         | Spring Monitor     |





